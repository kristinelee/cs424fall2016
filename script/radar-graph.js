//RadarChart

function RadarChart(data){
  this.data = data;
  var rect = document.getElementById('radar-graph').getBoundingClientRect();
  var w = rect.width, h = 300;
  
  //setup
  var svg = d3.select('#radar-graph')
            .append('svg')
            .attr('width', w)
            .attr('height', h);

  var group = svg.append("g")
          .attr("transform", "translate("+ w/2 +","+ h/2 +")");

  this.draw = function(song){
    if(!song){return;}

    var title = song.title;
    var lyrics = song.lyrics;
    var emotions = {anticipation: 0, fear: 0, sadness: 0, anger: 0, disgust: 0, surprise: 0, trust: 0, joy: 0};

    for (var word in lyrics){     //for all lyrics of a song
      if (data[word]) {
          data[word].forEach(function(d){ //for emotions related to lyric
              emotions[d] += lyrics[word];//add value of word to object
          });
      }     
    }
    
    $('#radar-graph-title').text(title);

    //set values
    var levels = 4;
    var total = 8;
    var radians = 2 * Math.PI;
    var radius = Math.min(w/3,h/3);

    var maxVal =5;  //find top value of overall emotions
    for (var e in emotions){
      if (emotions[e] > maxVal){
        maxVal = emotions[e];
      }
    }

    var scale = d3.scaleLinear()
          .range([1, radius-5])
          .domain([0, maxVal]);

    //draw chart
    group.selectAll('*').remove();

    //axis
    for(var i = 0; i < total; i++){
      group.append("line")
        .attr("x1", 0)
        .attr("y1", 0)
        .attr("x2", (radius*Math.sin(i*radians/total)))
        .attr("y2", (radius*Math.cos(i*radians/total)))
        .attr("stroke", "#555")
        .attr("stroke-width", 1); 

      //axis lines
      for(var j = 1; j < levels; j++){
        group.append("line")
          .attr("x1", ((j/levels)*radius*Math.sin(i*radians/total)))
          .attr("y1", ((j/levels)*radius*Math.cos(i*radians/total)))
          .attr("x2", ((j/levels)*radius*Math.sin((i+1)*radians/total)))
          .attr("y2", ((j/levels)*radius*Math.cos((i+1)*radians/total)))
          .attr("stroke", "#ccc")
          .attr("opacity", 0.75)
          .attr("stroke-width", 1); 
      }
    }
    i = 0;
    var str="";
    for (e in emotions){
      var rx = Math.cos(i*radians/total);
      var ry = Math.sin(i*radians/total);

      var textnode = group.append('g')
        .attr("transform", "translate("+ (10+radius)*ry +","+ (15+radius)*rx +")")
        .attr('id', 'axis-'+e)
        .on('mouseover', function() {
          d3.select(this).selectAll('text')
              .text(emotions[this.id.split('-').slice(-1)]);
        })
        .on('mouseout', function() {
          d3.select(this).selectAll('text')
              .text(this.id.split('-').slice(-1));
        });
      textnode.append('circle')
        .attr('r', 25)
        .attr('fill','transparent');

      textnode.append('text')
        .text(e)
        .attr('pointer-events','none')
        .attr('fill', function() {
          color = d3.hsl(colors[e])
          color.s += 0.2;
          color.l = 0.4;
          return color.toString();
        })
        .attr('y', 5)
        .attr('text-anchor', function() {
          if (ry > 0.99) { return 'start' }
          else if (ry < -0.99) { return 'end' }
          return 'middle';
        });

      str += scale(emotions[e])*ry+ "," + scale(emotions[e])*rx + " ";
      i++;
    }

    // data
    group.append("polygon") //connects nodes and fill
      .attr("fill", "steelblue")
      .attr("fill-opacity", 0.6)
      .attr("stroke", "steelblue")
      .attr("stroke-width", 2)
      .attr("points", str);
  }

  this.resize = function(){ 
    $('#song-chart').closest('.col-md-6').width('45%');

    w = $('#song-chart').width()

    d3.select('#radar-graph svg')
        .attr('width', w)
      .select('g')
        .attr("transform", "translate("+ w/2 +","+ h/2 +")");

  }
}