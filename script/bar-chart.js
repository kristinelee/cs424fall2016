// BarChart
// --------
// Creates horizontal bar chart object w/emotion frequency for artist
// Accepts artist as string

function BarChart(datasets) {
  var songData = datasets[2];
  var data = datasets[3];
  var emotionData = datasets[4];
  var rect = document.getElementById('bar-chart').getBoundingClientRect();
  
  var w = rect.width,
      h = 300,
      barSeparation = 7,
      dy = 23;
  var svg = d3.select('#bar-chart')
              .append('svg')
              .attr('width', w)
              .attr('height', h);

  // draws the bar chart on the '#bar-chart' svg canvas
  this.draw = function(d) {
    var artist = d.artist,
        lyrics = d.lyrics;

    if (!data[artist]) { return; } // quit early if artist undefined

    // map song lyrics to emotions
    var emotionsBySong = emotions.map(function() { return 0; });
    if (lyrics) {
      for (var word in lyrics) {
        var e = emotionData[word];
        if (e) {
          e.forEach(function(emotion) {
            emotionsBySong[emotions.indexOf(emotion)] += lyrics[word];
          })
        }
      }
    }

    // map & sum all artist songs to emotions
    var emotionsByArtist = data[artist]
      .map(function(d) {
        return { 
          e:emotions[d.e],       // emotion
          n:d.n,                 // cumulative emotion count
          w:emotionsBySong[d.e]  // emotion count for specific song
        };
      })
      .sort(function(a,b) {
        if (a.n === b.n) { return 0; }
        return a.n < b.n ? 1 : -1;
      })

    this.updateArtist(artist, d.title, emotionsByArtist[0].e)

    var maxN = d3.max(emotionsByArtist, function(emotion) {
      return emotion.n;
    }) || 0;
    maxN = Math.max(maxN, 50);

    var scale = d3.scaleLinear()
        .range([0, Math.max(50,w-175)])
        .domain([0, maxN])

    var nbar = svg.selectAll('.nbar')
        .data(emotionsByArtist);

    nbar.exit().remove();
    nbar.enter()
      .append('rect')
        .attr('class','nbar')
        .attr('x', 100)
        .attr('y',0)
        .attr('height', dy)
        .merge(nbar)
        .attr('transform',function(d, i) {
          return 'translate(0,'+ (dy+barSeparation)*i +')';
        })
        .on('mouseover', function(d,i) {
          var tooltipArray = [d.w +' in song', d.n + ' overall'];
          for (var m=0; m<2; m++) {
            var translateY = (dy+barSeparation)*i + m*15;
            svg.append('text')
                .text(tooltipArray[m])
                .style('font-size', '0.8em')
                .attr('class','bar-mouseover')
                .attr('x', 105 + scale(d.n))
                .attr('y', dy/2.5 )
                .attr('transform','translate(0,'+ translateY +')')
                .attr('fill',d3.hsl(colors[d.e]).darker().toString());
          }
        })
        .on('mouseout', function() {
          svg.selectAll('.bar-mouseover').remove();
        })
        .transition()
          .attr('width', function(d) { return scale(d.n) })
          .attr('fill', function(d) { 
            return d3.hsl(colors[d.e]).brighter().toString(); 
          });

    var wbar = svg.selectAll('.wbar')
        .data(emotionsByArtist);

    wbar.exit().remove();
    wbar.enter()
      .append('rect')
        .attr('class','wbar')
        .attr('pointer-events','none')
        .attr('x', 100)
        .attr('y',0)
        .attr('height', dy)
      .merge(wbar)
        .attr('transform',function(d, i) {
          return 'translate(0,'+ (dy+barSeparation)*i +')';
        })
        .transition()
          .attr('width', function(d) { return scale(d.w) })
          .attr('fill', function(d) { 
            return colors[d.e];
          });

    var text = svg.selectAll('text')
        .data(emotionsByArtist);

    text.exit().remove();
    text.enter()
      .append('text')
        .style('font-size', '0.9em')
        .attr('x', 90)
        .attr('y', dy/1.5)
        .attr('text-anchor','end')
      .merge(text)
        .attr('transform',function(d, i) {
          return 'translate(0,'+ (dy+barSeparation)*i +')';
        })
        .text(function(d) { return d.e })
  }

  // update the artist info bar
  this.updateArtist = function(artist, song, e) {
    var songsByArtist = songData.filter(function(d) {
      return d.artist === artist;
    }).map(function(d) {
      return d.title;
    })

    // update title info
    d3.select('#bar-chart-title')
      .text(artist)
      .append('span')
        .attr('class','ui-trigger')
        .style('color','gray')
        .style('cursor','pointer')
        .text(' (' + songsByArtist.length + ' songs)')
        .on('click', function() {
          // open info box
          d3.select('#bar-chart-info')
              .style('display','block');
        });

    // close info box
    d3.select('#close-info')
      .on('click', function() {
        d3.select('#bar-chart-info')
          .style('display','none');
      });

    // update info box
    var songs = d3.select('#bar-chart-info')
        .style('display','none')
      .selectAll('span')
        .data(songsByArtist);

    songs.exit().remove();

    var self = this;
    songs.enter().append('span')
        .style('margin','5px')
        .style('cursor','pointer')
      .merge(songs)
        .text(function(d) { return d })
        .style('color','gray')
        .on('mouseover', function() {
          this.style.color = 'red';
        })
        .on('mouseout', function() {
          this.style.color ='gray';
        })
        .on('click', function(e) {
          var d = songData.find(function(song) {
            return song.title === e && song.artist === artist;
          })
          if (d) {
            d3.select('#bar-chart').dispatch('songSelected', {detail: d});
          }
        })
  }

  // removes the bar chart from the '#bar-chart' svg canvas
  this.remove = function() {
    svg.remove();
  }

  this.resize = function() {
    $('#artist-chart').closest('.col-md-6').width('45%');
    w = $('#artist-chart').width();
    d3.select('#bar-chart svg')
        .attr('width', w)
  }
}