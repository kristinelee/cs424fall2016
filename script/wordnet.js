// Wordnet visualization:
// a tree that collapses/expands nodes on demand
// partly adapted from http://stackoverflow.com/questions/38011393/
function Wordnet(datasets, svg) {
  var emoTree = datasets[0];
  var emoLex = datasets[1];
  var songs = datasets[2];

  // init root data structure
  this.root = (function() {

      // transform data into a hierarchy-readable format
      // root > general emotion > word > song containing word
      var tree = {};
      tree.children = Object.keys(emoTree).map(function(category) {
        var emotion = {name: category};
        emotion.size = 0;
        emotion.children = emoTree[category]
          .map(function(word) {
            var child = { name:word, size:0 };
            if (Object.prototype.toString.call(emoLex[word]) === '[object Array]') {
              child.children = emoLex[word].map(function(index) { 
                return songs[index];
              })
              .sort(function(a,b) {
                if (a.lyrics[word] == b.lyrics[word]) { return 0; }
                return a.lyrics[word] < b.lyrics[word] ? 1 : -1;
              });
              child.size = child.children.length;
              emotion.size += child.size;
            }
            return child;
          })// end map word--song
          .sort(function(a,b) {
            if (a.size == b.size) { return 0; }
            return a.size < b.size ? 1 : -1;
          })// sort by number of songs containing word

        return emotion;
      })// end map emotion--word

      // constrain data.size
      var catScale = d3.scaleLinear().range([10, 40])
          .domain( d3.extent(tree.children.map(function(d) {return d.size})) );
      var wordScale = d3.scaleLinear().range([8, 30])

      tree.children.forEach(function(category) {
        category.size = catScale(category.size);
        wordScale.domain(d3.extent(category.children
            .map(function(d) {
              return d.size;
            }) 
          ));
        category.children.forEach(function(word) {
          word.size = wordScale(word.size);
        })
      })

      return d3.hierarchy(tree);
    })()

  // init force simulation
  svg.append('g')
    .attr('class','linkGroup')
  svg.append('g')
    .attr('class','nodeGroup')
  this.svg = svg;
  this.links = svg.selectAll('.link').data([]);
  this.nodes = svg.selectAll('.node').data([]);

  var self = this;
  this.simulation = d3.forceSimulation()
      .force('link', d3.forceLink().id(function(d) { return d.id }))
      .force('charge', d3.forceManyBody()
            .strength(function(d) {
              return (d.depth == 1) ? -1200 : -900;
            })
            .distanceMax(200) )
      .force('center', d3.forceCenter())
      .on('tick', function() {
        self.links
            .attr('x1', function(d) {return d.source.x})
            .attr('y1', function(d) {return d.source.y})
            .attr('x2', function(d) {return d.target.x})
            .attr('y2', function(d) {return d.target.y})

        self.nodes
          .attr('transform', function(d) {
            return 'translate('+d.x+','+d.y+')';
          })
      });

  this.flat = this._flatten();
  this.update();
}

Wordnet.prototype = {
  constructor: Wordnet,
  
  // get lastClick() {
  //   return this.clicked;
  // },
  // get selection() {
  //   return (this.selected) ? this.selected : null;
  // },

  //----- update functions to handle data changes -----//
  update: function(refilterData) {

    // filter tree by visible nodes (can optionally be skipped)
    if (refilterData === false && this.nodeData && this.linkData) {
    }
    else {
      var self = this;
      this.nodeData = this.flat.filter(function(node) {
          if (node.depth < 2) {
            return true;
          }
          var p = node.parent;
          if (p && p.expanded) {
            // constrain # child nodes
            var i = p.children.indexOf(node, p.start);
            return i < (p.start || 0) + 15 && i > -1;
          }
          return false;
        });
      this.linkData = this.root.links().filter(function(link) {
        return self.nodeData.indexOf(link.target) > -1;
      });
    }

    this._updateLinks(this.linkData);
    this._updateNodes(this.nodeData);

  },
  _updateLinks: function(linkData) { 
    this.simulation.force('link')
        .links(linkData);
    this.links = this.svg.select('.linkGroup').selectAll('.link')
      .data(linkData, function(d) {
        return d.target.id
      })

    this.links.exit()
      .transition()
        .style('stroke','transparent')
        .duration(300)
      .remove();

    var newLinks = this.links.enter().append('line')
        .attr('class','link')
        .attr('fill','transparent')
      .transition()
        .attr('fill','steelblue')

    this.links = this.links.merge(newLinks);
  },
  _updateNodes: function(nodeData) {
    var self = this;
    this.simulation.nodes(nodeData);
    this.nodes = this.svg.select('.nodeGroup').selectAll('.node')
      .data(nodeData, function(d) {
        return d.id
      })

    // animate exiting nodes
    var exit = this.nodes.exit();
    exit.selectAll('circle')
      .transition()
        .attr('r', 0)
        .duration(300);
    exit.selectAll('text')
      .transition()
        .attr('fill', 'transparent')
        .duration(300);
    exit.transition().delay(300).remove();

    // attach listeners to entering nodes
    var newNodes = this.nodes.enter().append('g')
        .attr('class', 'node')
        .on('click', this._clicked.bind(this))
        .call( 
          d3.drag()
            .on('start', this._dragStarted.bind(this))
            .on('drag', this._dragged)
            .on('end', this._dragEnded.bind(this)) )

    newNodes.append('circle')
    newNodes.append('text')
        .attr('fill','transparent')

    // data merge
    this.nodes = this.nodes.merge(newNodes);

    this.nodes.selectAll('circle')
      .transition()
        .attr('r',function(d) {
          if (d.depth == 0) {
            return 7;
          }
          else if (d.expanded) {
            return 0;
          }
          else {
            return d.data.size || 10;
          }
        })
        .attr('fill', function(d, i,j ) {
          return self._color(d);
        });
    this.nodes.selectAll('text')
        .text(function(d) { return d.data.name || d.data.title; })
        .style('pointer-events', function(d) {
          return (d.depth == 3 || d.expanded) ? 'auto' : 'none';
        })
        .transition()
          .attr('fill', function(d) {
            if (self.selected && self.selected == d) {
              return 'red';
            }
            var a = 1.25-0.25*d.depth;
            return 'rgba(0,0,0,' + a +')';
          })
    this.nodes.selectAll('text')
        .on('mouseover', this._hovered.bind(this))
        .on('mouseout', this._unhovered.bind(this));
  },

  // return the color of a node based on its ancestor & depth
  _color: function(node) {
    var depth = node.depth;

    if (depth > 2) { return 'transparent'; }

    while (node.depth > 1) {
      node = node.parent;
    }
    var color = colors[node.data.name] || 'gray';

    if (depth > 1) {
      return d3.hsl(color).brighter().toString();
    }
    else {
      return color;
    }
  },

  // flatten hierarchical data to flat array for force layout
  _flatten: function() {
    var nodes = [], i = 0;
    function recurse(node) {
      if (node.children) node.children.forEach(recurse);
      if (!node.id) node.id = ++i;
      else ++i;
      nodes.push(node);
    }
    recurse(this.root);
    return nodes;
  },

  // set "expanded" to false on node and all descendants
  _collapseNodes: function(nodes, reset) {
    var self = this;
    nodes.forEach(function(child) {
      if (reset) {
        child.start = 0;
      }
      if (child.children) {
        self._collapseNodes(child.children, reset);
      }
      child.expanded = false;
    })
  },

  // expand ancestors of node
  _expandNode: function(child) {
    while (child.parent.depth > 0) {
      var parent = child.parent;
      parent.expanded = true;
      var i = parent.children.indexOf(child);
      var startIndex = parent.start || 0;
      // node is not already expanded
      if (startIndex + 15 <= i || startIndex > i) {

        // collapse children
        this._collapseNodes(parent.children.slice(startIndex, startIndex+15));
        if (i + 15 < parent.children.length) {
          parent.start = i;
        }
        else {
          i = parent.children.length - 15;
        }
      }
      child = parent;
    }    
  },

  //----- event handling ---------- //

  // fire clickFired event when a node is clicked or dragged
  _setLastClick: function(d) {
    this.clicked = d;
    this.svg.dispatch('clickFired', {detail: d});
  },

  // fire selectionFired event when a song is selected/deselected
  _setSelection: function(d, update) {
    if (this.selected && this.selected == d) {
      this.selected = null;
    }
    else {
      this.selected = d;
    }
    this.update(update == true);
    this.svg.dispatch('selectionFired', {detail: d});
  },

  // set the node to be displayed in the ui
  _setUINode: function(d) {
    if (!(d.expanded || (d.parent && d.parent.expanded))) {
      $('.ui-box').hide();
    }
    else {
      var parent = (d.expanded) ? d : d.parent;
      this._uiNode = parent;
      this._updateUI();
    }
  },
  _updateUI: function() {
    if (!this._uiNode) {
      console.log('failed call to _updateUI')
      return;
    }
    var node = this._uiNode;
    var start = (node.start || 0);
    var end = start + 15;
    var sz = node.children.length;

    $('.ui-box').show();
    $('.wordnet-selection').text(node.data.name);
    $('.wordnet-slider-info').text(function() {
      return (start+1) + '-' + end + ' / ' + sz;
    });
  },

  // receive click event from external ui button
  // modify constraints on which children are displayed
  uiClicked:function(e) {
    if (!this._uiNode) {
      console.log('failed call to onUIClick', e)
      return;
    }

    var node = this._uiNode;
    var start = node.start || 0;
    var sz = node.children.length;

    var id = e.target.id;
    if (id == 'wordnet-slider-right') {
      // increase index of first child displayed
      if (start + 15 == sz) {
        node.start = 0;
      }
      else if (start + 15 > sz - 15 && sz > 15) {
        node.start = sz - 15;
      }
      else {
        node.start = start + 15;
      }
    }
    else {
      // decrease index of first child displayed
      if (start == 0 && sz > 15) {
        node.start = sz - 15;
      }
      else if (start < 15) {
        node.start = 0;
      }
      else {
        node.start = start - 15;
      }
    }

    for (var i = start; i < start+15; ++i) {
      this._collapseNodes([node.children[i]]);
    }
    this._updateUI();
    this.update();
    this.simulation
      .force('center', d3.forceCenter())
      .alpha(0.1)
      .restart();
    this._setLastClick(this._uiNode);
  },

  // receive event from other vis
  // find the nearest node to current selection
  findNode: function(song) {
    var current = this.selected;
    var matches = this.flat.filter(function(d) {
      return d.data === song;
    })

    function searchByFilter(func) {
      var results = matches.filter(func);
      if (results.length > 0) {
        matches = results;
        return true;
      }
      return false;
    }

    // perform a series of increasingly narrow searches
    searchByFilter(function(d) {
      return d.parent.parent === current.parent.parent;
    }) &&
    searchByFilter(function(d) {    // search children of expanded nodes
      return d.parent.expanded;
    }) &&
    searchByFilter(function(d) {    // search siblings of current selection
      return d.parent === current.parent;
    })

    var match = matches[0];
    if (match == current) { return; } // exit

    // collapse tree and set selection to matched node
    // this._collapseNodes(this.root.children);
    this._setUINode(match);
    this.select(match);
    this._setLastClick(match.parent);
  },

  select:function(song) {
    this._expandNode(song);
    this._setSelection(song, true);
    this.simulation.alpha(0.1).restart();
  },

  resize:function() {
    $('#wordnet-chart').closest('.row').css('display','block');

    var rect = document.getElementById('wordnet').getBoundingClientRect();
    d3.select('#wordnet>svg')
        .attr('width',rect.width)
    d3.select('#wordnetSvg')
        .attr('transform','translate('+rect.width/2+',225)');

  },

  //----------- mouse events ------------- //

  // click: expand, collapse, or select a node
  _clicked: function(d) {
    var self = this;
    // select a parent node to be expanded/collapsed
    if (d.children) {
      d.expanded = (d.depth > 0) ? !d.expanded : false;

      // collapse node AND descendants
      if (!d.expanded) {
        this._collapseNodes(d.children, d.depth == 0);
      }
      this.update();
      this._setUINode(d);
    }
    else {
      // select/deselect a leaf node
      this._setSelection(d);
    }
  },
  _hovered:function(d) {
    // visual feedback when song is highlighted/selected
    var text = d3.event.target;
    if (this.selected && this.selected == d) {
      text.setAttribute('fill', 'red');
    }
    else {
      text.setAttribute('fill','black');
    }
  },
  _unhovered:function(d) {
    var text = d3.event.target;
    if (this.selected && this.selected == d) {
      text.setAttribute('fill', 'red');
    }
    else {
      text.setAttribute('fill','rgba(0,0,0,'+ (1.25-0.25*d.depth) +')');
    }
  },
  _dragStarted: function(d) {
    this._setLastClick(d);
    this._setUINode(d);
    if (!d3.event.active) {
      this.simulation.alphaTarget(0.3).restart()
    }
  },
  _dragged: function(d) {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
  },
  _dragEnded: function(d) {
    if (!d3.event.active) {
      this.simulation.alphaTarget(0);
    }
    d.fx = null;
    d.fy = null;
  }
}