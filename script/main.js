const lyricData = 'src/songs.min.json';             //lyric dataset
const emoTreeData = 'src/emotions-by-cat.min.json'; //emotion dataset
const emoLexData = 'src/emotions-by-word.min.json'; //emotion-lyric association
const emoArtistData = 'src/emotions-by-artist.min.json';//emotion-artist data
const emoWordData = 'src/categories_by_emotion.json';//reverse emotion data


// null values where positive and negative emotions were removed
const emotions = [
  'anticipation',
  'joy',
  null,
  'surprise',
  'trust',
  'fear',
  'anger',
  'disgust',
  null,
  'sadness'
];
const colors = {
  'anticipation':'#efef33', 
  'joy':'#fa4', 
  'surprise':'#5ec', 
  'trust':'#e59', 
  'fear':'#b7c', 
  'anger':'#e65',
  'disgust':'#7d4', 
  'sadness':'#6ae'
};

$('#wordnet-chart').hide();
$('#mood-chart').hide();
$('#artist-chart').hide();
$('#song-chart').hide();

var wordnet, barChart, radarChart, stackedBar;

d3.queue()
  .defer(d3.json, emoTreeData)
  .defer(d3.json, emoLexData)
  .defer(d3.json, lyricData)
  .defer(d3.json, emoArtistData)
  .defer(d3.json, emoWordData)
  .awaitAll(function(error, datasets) {     
    if (error) { 
      console.log('error',error);
      return;
    }

    // wordnet
    var rect = document.getElementById('wordnet').getBoundingClientRect();
    var wordnetSvg = d3.select('#wordnet').append('svg')
        .attr('width','100%')
        .attr('height',450)
      .append('g')
        .attr('id', 'wordnetSvg')
        .attr('transform','translate('+rect.width/2+',225)');
    wordnet = new Wordnet(datasets, wordnetSvg);
    $('#wordnet button').on('click', wordnet.uiClicked.bind(wordnet));
    $('#wordnet-chart').show();
    wordnet.resize();

    // barchart
    barChart = new BarChart(datasets);
    stackedBar = new StackedBar(datasets[4]);
    radarChart = new RadarChart(datasets[4]);

    wordnetSvg
      .on('clickFired', function() {
        var click = d3.event.detail;
        if (click.depth >= 2) {
          $('#mood-chart').show();
          stackedBar.resize();
          stackedBar.draw(click);
        }
      })
      .on('selectionFired', function() {
        var selection = d3.event.detail;
        if (selection && selection.data) {
          $('#artist-chart').show();
          barChart.resize();
          barChart.draw(selection.data);

          $('#song-chart').show();
          radarChart.resize();
          radarChart.draw(selection.data);
        }
      });

    d3.select('#small-multiples')
      .on('songSelected', function() {
        wordnet.select(d3.event.detail);
      })

    d3.select('#bar-chart')
      .on('songSelected', function() {
        wordnet.findNode(d3.event.detail);
      })
  });
// resize event
var debouncedResize = _.debounce(function() {
    // resize & center wordnet visualization
    if (wordnet) {
      wordnet.resize();
    }

    // resize bar visualization
    if (barChart) {
      barChart.resize();
    }
  }, 300);
window.addEventListener('resize', debouncedResize);