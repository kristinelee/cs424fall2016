function StackedBar(data) {
  var rect = document.getElementById('small-multiples').getBoundingClientRect();
  var w = rect.width, h = 400;
  var offset = 120;
  var barSeparation = 2;
  var svg = d3.select('#small-multiples')
    .append('svg')
      .attr('width', w)
      .attr('height', h)
    .append('g')
      .attr("transform", "translate("+offset+", 60)");

  // draws the bar chart on the '#small-multiples' svg canvas
  this.draw = function(node) {
    // validate node is of depth 3 or 2 with children
    if (node.depth < 2) { return; }
    if (node.depth === 3) { node = node.parent; }
    if (!node.children || node.children.length == 0) { return;}

    // update header info
    var startIndex = node.start || 0;
    var children = node.children.slice(startIndex, startIndex + 15);
    var header = d3.select('#small-multiples-title')
        .text((startIndex+1) + '-' + (startIndex+15)+' songs containing "'+node.data.name + '"')
      .append('div');

    header.append('span')
        .style('display','inline-block')
        .style('width', '10px')
        .style('height', '10px')
        .style('margin-right','5px')
        .style('border', '2px solid '+colors[node.parent.data.name]);

    header.append('span')
        .text('- occurrences of "' +node.data.name +'"')
        .style('font-size', '0.8em')

    // parse data into layers
    var input = children.map(function(d) {
      var o = {
        "key": d.data.title,
        "anticipation": 0,
        "fear": 0,
        "sadness": 0,
        "anger": 0,
        "disgust": 0,
        "surprise": 0,
        "trust": 0,
        "joy": 0
      }

      for (var word in d.data.lyrics){     //for all lyrics of a song
        if (data[word]) {
            data[word].forEach(function(emotion){
                o[emotion] += d.data.lyrics[word];//add value of word to object
            });
        }     
      }

      return o;
    })

    if (!input || !input.length) { console.log('error', node, input); return; }

    // sort layers by leading emotion
    var emotions = Object.keys(input[0])
      .filter(function(key) { 
        return key !== 'key';
      })
      .sort(function(a,b){
        if (a === node.parent.data.name) return -1;
        if (b === node.parent.data.name) return 1;
        if (input[0][a] === input[0][b]) return 0;
        return (input[0][a] < input[0][b]) ? 1 : -1;
      })

    var n = 8, // number of layers (emotions)
      m = input.length, // number of samples per layer
      labels = input.map(function(d,i) {
        var label = d.key;
        if (label && label.length > 15) {
          label = label.slice(0,15) + '...';
        }
        return (startIndex+i+1) + '. ' +label;
      }),
  
      //go through each layer (pop1, pop2 etc, that's the range(n) part)
      //then go through each object in data and pull out that objects's population data
      //and put it into an array where x is the index and y is the number
      layers = (d3.range(n).map(function(d) { 
                var a = [];
                for (var i = 0; i < m; ++i) {
                    a[i] = {
                        x: i, 
                        y: input[i][emotions[d]],
                        y0: 0
                    };
                }
                return a;
             }));

    // create the stacks
    var stack = d3.stack(layers);
    layers.forEach(function(layer, i) {
      if (i > 0) {
        layer.forEach(function(bar, j) {
          var prevBar = layers[i-1][j];
          bar.y0 = prevBar.y0 + prevBar.y;
        })
      }
    });

    //the largest stack
    var yStackMax = d3.max(layers, function(layer) { return d3.max(layer, function(d) { return d.y0 + d.y; }); });

    var y = d3.scaleBand()
        .domain(d3.range(m))
        .range([0, h-70])
        .padding(0.3)
        .round(.08);

    var x = d3.scaleLinear()
        .domain([0, yStackMax])
        .range([0, w - offset - 20]);

    var layer = svg.selectAll(".layer")
        .data(layers);

    layer.exit().remove();

    layer = layer.enter().append('g')
        .attr('class','layer')
      .merge(layer);
    
    layer.transition()
        .attr('fill', function(d,i) {
          return colors[emotions[i]];
        })

    // draw stacked bars
    var rect = layer.selectAll('.rect')
        .data(function(d) { return d; })

    rect.enter().append('rect')
        .attr('class','rect')
      .merge(rect)
        .attr('y', function(d) { return y(d.x); })
        .attr("height", y.bandwidth())
        .transition()
          .attr("x", function(d) { return x(d.y0); })
          .attr("width", function(d) { return x(d.y); });

    // highlight the frequency of the currently selected node
    var mainColor = d3.hsl(colors[node.parent.data.name]);
    mainColor.opacity = 0.2;
    mainColor.l -= 0.2;
    mainColor = mainColor.toString();

    var currentRect = layer.selectAll('.current')
        .data(children.map(function(d) { 
          return d.data.lyrics[node.data.name]
        }))
    currentRect.enter().append('rect')
        .attr('class','current')
      .merge(currentRect)
        .attr('x', 0)
        .attr('y', function(d, i) { return y(i); })
        .attr('height', y.bandwidth())
        .attr('fill', 'transparent')
        .attr('stroke',mainColor)
        .transition()
          .attr("width", function(d) { return x(d); });

    // update axis labels
    var yAxis = d3.axisLeft()
        .scale(y.domain(labels))
        .ticks(m)
        .tickSize(2);

    svg.selectAll('g.yAxis').remove();

    svg.append("g")
        .attr("class", "yAxis")
        .call(yAxis);

    $('#mood-chart').show('slow');  

    // fire selection from axis label
    d3.select('.yAxis').selectAll('.tick')
        .on('mouseover', function() {
          d3.select(this)
              .style('cursor','pointer')
            .selectAll('text')
              .attr('fill','red')
        })
        .on('mouseout', function() {
          d3.select(this)
            .selectAll('text')
              .attr('fill','black')
        })
        .on('click',function() {
          var ticks = this.parentNode.querySelectorAll('.tick');
          var i = [].slice.call(ticks).indexOf(this);
          if (i > -1) {
            d3.select('#small-multiples')
              .dispatch('songSelected', {detail: children[i]})
          }
        })

  }


  // removes the bar chart from the '#bar-chart' svg canvas
  this.remove = function() {
    svg.remove();
  }

  this.resize = function() {
    $('#mood-chart').closest('.row').css('display','block');
    w = $('#mood-chart').width()
    d3.select('#small-multiples svg')
        .attr('width', w)
  }
}